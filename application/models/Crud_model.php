<?php
class Crud_model extends CI_Model
{
    public function insertData($data)
    {
        return $this->db->insert('employee',$data);
    }
// read/display table content
    public function readTable()
    {
        return $this->db->select('*')
        ->get('employee')
        ->result_array();
    }
    public function deleteData($id)
    {
        return $this->db->where('id',$id)
        ->delete('employee');
    }

    public function editRecord($id)
    {
        return $this->db->select('*')
        ->where('id',$id)
        ->get('employee')
        ->row_array();
    }

    public function updateData(array $data,$id)
    {
        return $this->db->set($data)
        ->where('id',$id)
        ->update('employee');
        
    }
}
?>