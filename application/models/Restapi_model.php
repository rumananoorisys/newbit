<?php
  class Restapi_model extends CI_Model
  {
     public function insertRecord($data)
     {
         return $this->db->insert('employee',$data);
     }

    //  delete query
    public function deleteRecord($id)
    {
      return $this->db->where('id',$id)
      ->delete('employee');
    }

    // update query
    public function updateRecord(array $data,$id)
    {
      return $this->db->set($data)
      ->where('id',$id)
      ->update('employee');
    }
  }
?>