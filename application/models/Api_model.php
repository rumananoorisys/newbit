<?php
class Api_model extends CI_Model
{
    // insert record in table
   public function insertInfo(array $data){
       return $this->db->insert('employee',$data);
   }
//    delete record
   public function deletedInfo($id)
   {
      return $this->db->where('id',$id)
      ->delete('employee');
   }
//    update record
    public function updatedInfo(array $data,$id)
    {
        return $this->db->set($data)
        ->where('id',$id)
        ->update('employee');
    }
}

?>