
<?php
class Product_model extends CI_Model
{
    public function readTable()
    {
    return $this->db->select('*')
    ->get('product')
    ->result_array();
    }

    public function insertData($data)
    {
        return $this->db->insert('product',$data);
    }

    public function deleteRecord($id)
    {
        return $this->db->where('id',$id)
        ->delete('product');
    }

    // public function editRecord($id)
    // {
    //    return $this->db->select("*")
    //    ->where('id',$id)
    //    ->get('product')
    //    ->row_array();
    // //    echo $this->db->last_query(); die();

    // }

    // public function updateRecord(array $data,$id)
    // {
    //     return $this->db->set($data)
    //     ->where('id',$id)
    //     ->update('product');
    // }



    public function editRecord($id)
    {
        return $this->db->select("*")
        ->where('id',$id)
        ->get('product')
        ->row_array();
    }

    public function updateData($data,$id)
    {
        return $this->db->set($data)
        ->where('id',$id)
        ->update('product');
    }


    public function checkname($name)
    {
       return $this->db->where('product_name',$name)
       ->get('product')
       ->row_array();
    }
} 