<?php
class Crud extends CI_Controller
{
     public function __construct()
     {
          parent::__construct();
         $this->load->model('Crud_model','c');
     }


    public function inserRecord()
    {
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $branch = $this->input->post('branch');

        $this->form_validation->set_rules('name','Name','required|alpha');

        if($this->form_validation->run())
        {
            $data = [
                'name'=>$name,
                'email'=>$email,
                'branch'=>$branch
            ];
    
            $res = $this->c->insertData($data);
    
            if($res==true)
            {
                echo "inserted";
            }else
            {
                echo "Error";
            }
    
        }else{
            $this->session->set_flashdata("error","Inavlid Input");        
             redirect(base_url('insertForm'));
        }           
    }
      public function showForm()
      {
          $this->load->view('Showform_view');
      }

      public function readTable()
      {
        $res['r'] = $this->c->readTable();
        $this->load->view('Crud_view',$res);


      }

      public function deleteRecord($id)
      {
         $res = $this->c->deleteData($id);
         if($res)
         {
            redirect(base_url('showTable'));
         }else
         {
             echo "Try Again";
         }
      }

      public function editData($id)
      {
          $res['u'] = $this->c->editRecord($id);
          $this->load->view('Crudupdate_view',$res);
      }

      public function updateInfo($id)
      {
           $name = $this->input->post('name');
           $email = $this->input->post('email');
           $branch = $this->input->post('branch');
           
           $data = [
               'name'=>$name,
               'email'=>$email,
               'branch'=>$branch
           ];

            $res = $this->c->updateData($data,$id);

            if($res==true)
            {
                redirect(base_url('showTable'));
            
            }else{
                echo "try again";
            }
      }
}
