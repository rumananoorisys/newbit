<?php
class Crudproduct extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Product_model', 'p');
    }
    public function showTable()
    {
        $res['a'] = $this->p->readTable();
        $this->load->view('Crudproduct_view', $res);
    }
    public function showForm()
    {
        $this->load->view('Productform_view');
    }

    public function getForm()
    {
        $name = $this->input->post('product_name');
        $stock = $this->input->post('product_instock');
        $code = $this->input->post('product_code');

        $data = [
            'product_name' => $name,
            'product_instock' => $stock,
            'product_code' => $code

        ];

        $res = $this->p->insertData($data);

        if ($res == true) {
            echo "inserted";
        } else {
            echo "Try again";
        }
    }

    public function deleteData($id)
    {
        $res = $this->p->deleteRecord($id);

        if ($res == true) {
            echo "deleted";
        } else {
            echo "Try again";
        }
    }

    //   public function editData($id)
    //   {
    //     //   echo json_encode($id); die();
    //       $res['u'] = $this->p->editRecord($id);
    //       $this->load->view('updateForm_view',$res);
    //   }



    //   public function updateData($id)
    //   {
    //       $name = $this->input->post('product_name');
    //       $stock = $this->input->post('product_instock');
    //       $code = $this->input->post('product_code');

    //       $data = [
    //           'product_name'=>$name,
    //           'product_instock'=>$stock,
    //           'product_code'=>$code
    //       ];

    //       $res = $this->p->updateRecord($data,$id);
    //       if($res==true)
    //       {
    //           echo "updated";
    //       }else
    //       {
    //           echo "Try Again?????";
    //       }

    //   }

    public function edit($id)
    {
        $res['u'] =  $this->p->editRecord($id);

        $this->load->view('updateForm_view', $res);
    }

    public function updateRecord($id)
    {
        $name = $this->input->post('product_name');
        $stock = $this->input->post('product_instock');
        $code = $this->input->post('product_code');

        $data = [
            'product_name' => $name,
            'product_instock' => $stock,
            'product_code' => $code,
        ];

        $res = $this->p->updateData($data, $id);
        if ($res == true) {
            echo "updated";
        } else {
            echo "Try Again????????";
        }
    }

    public function logindisplay()
    {
        $this->load->view('loginform');
    }

    public function fetchlogin()
    {
        $name = $this->input->post('name');
        $this->form_validation->set_rules('name', 'Name', 'required');
        if ($this->form_validation->run()) {
            $res = $this->p->checkname($name);
            if ($res) {
                echo "logged in";
                $session_data=[
                    'product_name'=> $name
                ];
                $this->session->set_userdata($session_data);
            } else {
                echo "Try Again";
            }
        }
    }

    public function logout()
    {
      $res =  $this->session->unset_userdata('product_name');
      echo json_encode($res);
    }



}
