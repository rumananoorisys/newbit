<?php
class Api extends CI_Controller
{
  public function __construct()
  {
     parent::__construct();
     $this->load->model('Api_model','a');
  }
//  public function a(){
//      echo "hi";
//  }

  public function insertInfo()
  {
      $name = $this->input->post('name');
      $email = $this->input->post('email');
      $branch = $this->input->post('branch');

      $data=[
          'name'=>$name,
          'email'=>$email,
          'branch'=>$branch
      ];
    //   echo json_encode($data);
    $res = $this->a->insertInfo($data);

    if($res==true){
        $response = [
            'status'=>'success',
            'message'=>'your record inserted'
        ];
    }else{
        $response = [
            'status'=>'fail',
            'message'=>'fail to inserted'
        ]; 
    }
    echo json_encode($response);
  }
// Delete records
  public function deleteInfo()
  {
      $id = $this->input->post('id');

      $res = $this->a->deletedInfo($id);

      if($res==true){
          $response = [
              'status'=>'success',
              'message'=>'your record is deleted'
          ];
      }else{
          $response = [
              'status'=>'failed',
              'message'=>'try again'
          ];
      }
      echo json_encode($response);
  }
//    update function
  public function updateInfo()
  {
      $id = $this->input->post('id');
      $name = $this->input->post('name');
      $email = $this->input->post('email');
      $branch = $this->input->post('branch');

    //   query to select row to be updated
      $sql = $this->db->select('*')
      ->where('id',$id)
      ->get('employee')
      ->row();
      
      $nameold = $sql->name;
      $emailold = $sql->email;
      $branchold = $sql->branch;
// echo json_encode($nameold); die();
      $data= [
          'name'=>isset($name)?$name:$nameold,
          'email'=>isset($email)?$email:$emailold,
          'branch'=>isset($branch)?$branch:$branchold
      ];

      $res = $this->a->updatedInfo($data,$id);

      if($res==true)
      {
          $response = [
              'status'=>'success',
              'message'=>'updated successfully!'
          ];
      }else
      {
          $response = [
              'status'=>'failed',
              'message'=>'Try again!'
          ];
      }
      echo json_encode($response);


  }
  
      
  
}
?>