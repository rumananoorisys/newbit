<?php
class Restapi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        
        $this->load->model('Restapi_model','r');
    }  
    
    public function insertData()
    {
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $branch = $this->input->post('branch');

        $data = [
            'name'=>$name,
            'email'=>$email,
            'branch'=>$branch
        ];
        $res = $this->r->insertRecord($data);

        if($res==true)
        {
            $response = [
                'status'=>'success',
                'message'=>'Record Inserted Successfully!!'
            ];
        }else{

            $response = [
                'status'=>'Error',
                'message'=>'Failed to Insert???'
            ];
        }

        echo json_encode($response);
    }
    // Delete Records
    public function deleteData()
    {
        $id = $this->input->post('id');

        $res = $this->r->deleteRecord($id);

        if($res==true)
        {
            $response = [
                'status'=>'success',
                'message'=>'Record Deleted Successfully!!!!!!!'
            ];
        }else
        {
            $response = [
                'status'=>'Error',
                'message'=>'Try Again??????'
            ];
        }
        echo json_encode($response);
    }
    // Update Record
    public function updateData()
    {
        $id = $this->input->post('id');
        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $branch = $this->input->post('branch');

        $sql = $this->db->select('*')
        ->where('id',$id)
        ->get('employee')
        ->row();

        $nameold = $sql->name;
        $emailold = $sql->email;
        $branchold = $sql->branch;

        $data = [
            'name' =>isset($name)?$name:$nameold,
            'email'=>isset($email)?$email:$emailold,
            'branch'=>isset($branch)?$branch:$branchold
            ];

            $res = $this->r->updateRecord($data,$id);

            if($res==true)
            {
                $response = [
                    'status'=>'success',
                    'message'=>'Record Updated successfullt!!!!!'
                ];
            }else
            {
                $response = [
                    'status'=>'Error',
                    'message'=>'Try Again???????????'
                ];
            }

            echo json_encode($response);
    }
}
?>


































