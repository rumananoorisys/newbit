<html>
    <head>
       <link rel="stylesheet" href="<?=base_url("assets\css\bootstrap.min (8).css")?>">
       
    </head>
   
    <form action="<?=base_url('Crud/updateInfo/').$u['id']?>" method="POST" role="form">
        <legend>Update Record</legend>
    
        <div class="form-group">
            <label for="">Name</label>
            <input type="text" class="form-control" id="name" name="name" value = "<?=$u['name']?>">
        </div>
        <div class="form-group">
            <label for="">Email</label>
            <input type="text" class="form-control" id="email" name="email" value = "<?=$u['email']?>" >
        </div>
        <div class="form-group">
            <label for="">Branch</label>
            <input type="text" class="form-control" id="branch" name="branch" value = "<?=$u['branch']?>">
        </div>
        <button type="submit" class="btn btn-primary" >Submit</button>
    </form>
    
</html>